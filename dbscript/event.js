const mongoose = require('mongoose')
const Event = require('../models/event')
mongoose.connect('mongodb://localhost:27017/example')
async function clear () {
  await Event.deleteMany({})
}

async function main () {
  await clear()
  await Event.insertMany([
    {
      title: 'Title 1', content: 'Content 1', startDate: new Date('2022-03-25 08:00'), endDate: new Date('2022-03-25 16:00'), class: 'event'
    },
    {
      title: 'Title 2', content: 'Content 2', startDate: new Date('2022-03-26 08:00'), endDate: new Date('2022-03-26 16:00'), class: 'event2'
    },
    {
      title: 'Title 3', content: 'Content 3', startDate: new Date('2022-03-27 08:00'), endDate: new Date('2022-03-27 16:00'), class: 'event3'
    },
    {
      title: 'Title 4', content: 'Content 4', startDate: new Date('2022-03-28 08:00'), endDate: new Date('2022-03-28 12:00'), class: 'event'
    },
    {
      title: 'Title 5', content: 'Content 5', startDate: new Date('2022-03-28 13:00'), endDate: new Date('2022-03-28 16:00'), class: 'event2'
    }

  ])
  Event.find({})
}

main().then(function () {
  console.log('Finish')
})
