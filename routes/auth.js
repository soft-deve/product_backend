const express = require('express')
const router = express.Router()
const User = require('../models/User')
const bcrypt = require('bcryptjs')
const { generateAccessToken } = require('../helpers/auth')

const login = async function (req, res, next) {
  const username = req.params.username
  const password = req.params.password
  try {
    const user = await User.findOne({ username: username }, ' -password').exec()
    const verifyResult = await bcrypt.compare(password, user.password)
    if (!verifyResult) {
      return res.status(404).json({
        massage: 'User not found!!!'
      })
    }
    const token = generateAccessToken({ _id: user._id, username: user.username })
    res.json({ user: { _id: user._id, username: user.username, roles: user.roles }, token: token })
  } catch (err) {
    return res.status(404).json({
      massage: err.massage
    })
  }
}
router.get('/', login)
module.exports = router
