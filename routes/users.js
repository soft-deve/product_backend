const express = require('express')
const router = express.Router()
const User = require('../models/User')
const getUsers = async function (req, res, next) {
  try {
    const Users = await User.find({}).exec()
    res.status(200).json(Users)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
const getUser = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await User.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        massage: 'User not found!!!'
      })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).json({
      massage: err.massage
    })
  }
}
const addUsers = async function (req, res, next) {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

const updateUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({ massage: err.massage })
  }
}

const deleteUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ massage: err.massage })
  }
}

router.get('/', getUsers)
router.get('/:id', getUser)
router.post('/', addUsers)
router.put('/:id', updateUser)
router.delete('/:id', deleteUser)

module.exports = router
